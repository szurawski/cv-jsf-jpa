package pl.codementors;



import javax.enterprise.inject.spi.CDI;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter(forClass = Tech.class, value = "Convertione")
public class TechConverter implements Converter {


    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        CvDataStore store = CDI.current().select(CvDataStore.class).get();
        if (s == null || s.equals("null")) {//No value or nothing selected in html.
            return null;
        }
        return store.getTechno().get(Integer.parseInt(s));
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        CvDataStore store = CDI.current().select(CvDataStore.class).get();
        if (o == null) {
            return "null";
        }
        return store.getTechno().indexOf((Tech) o)+"";//Return unique id which will be used in html form.
    }
}
