package pl.codementors;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "edukacja")
public class Edu implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String okresNauki;

    @Column
    private String szkola;

    @Column
    private String specjalnosc;

    Edu(){

    }

    @ManyToOne
    private Cv cv;

    public Edu(String okresNauki, String szkola, String specjalnosc) {
        this.okresNauki = okresNauki;
        this.szkola = szkola;
        this.specjalnosc = specjalnosc;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOkresNauki() {
        return okresNauki;
    }

    public void setOkresNauki(String okresNauki) {
        this.okresNauki = okresNauki;
    }

    public String getSzkola() {
        return szkola;
    }

    public void setSzkola(String szkola) {
        this.szkola = szkola;
    }

    public String getSpecjalnosc() {
        return specjalnosc;
    }

    public void setSpecjalnosc(String specjalnosc) {
        this.specjalnosc = specjalnosc;
    }
}
