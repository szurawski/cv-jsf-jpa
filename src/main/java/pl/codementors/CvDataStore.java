package pl.codementors;



import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ApplicationScoped
public class CvDataStore {

    private Cv cvs;
    private List<Tech> techno = new ArrayList<>();

    @PostConstruct
    public void init() {
        cvs = new Cv();

        cvs.setImie("Sebastian");
        cvs.setNazwisko("Żurawski");
        cvs.setTel("707034143");
        cvs.getZainteresowania().add("sport");
        cvs.getZainteresowania().add("komputery");
        cvs.getZainteresowania().add("muzyka");
        Tech java = new Tech("Java", Tech.Kategoria.JĘZYK);
        Tech jquery = new Tech("Jquery", Tech.Kategoria.BIBLIOTEKA);
        Tech spring = new Tech("Spring", Tech.Kategoria.FRAMEWORK);


        techno.addAll(Arrays.asList(java, jquery, spring));

        cvs.getUmiejetnosci().add(jquery);
        cvs.getUmiejetnosci().add(java);
        cvs.getUmiejetnosci().add(spring);


//        cvs.getUmiejetnosci().add("szybko biegam na 10m");
//        cvs.getUmiejetnosci().add("skacze nisko");
//        cvs.getUmiejetnosci().add("turlam się koślawo");
        cvs.getEdukacja().add(new Edu("1998-2006","Sp5 Kraśnik","podstawowe"));
        cvs.getDoswiadczenie().add(new Dosw("2008-2010","dotMedia","drukarz"));
    }

   public Cv getCV() {
       return Cloner.clone(cvs);

   }

    public List<Tech> getTechno() {
        return Cloner.clone(techno);
    }

    public void updateCv(Cv cv) {
        cvs = cv;
    }
}
