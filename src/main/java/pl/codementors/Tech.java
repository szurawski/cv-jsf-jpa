package pl.codementors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "technologia")
public class Tech implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String nazwaTechnologi;



    public enum  Kategoria{
        JĘZYK,
        FRAMEWORK,
        BIBLIOTEKA,
    }
    @Column
    private Kategoria kategoria;

    public Tech() {
    }

    @ManyToOne
    private Cv cv;

    public Tech(String nazwaTechnologi, Kategoria kategoria) {
        this.nazwaTechnologi = nazwaTechnologi;
        this.kategoria = kategoria;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNazwaTechnologi() {
        return nazwaTechnologi;
    }

    public void setNazwaTechnologi(String nazwaTechnologi) {
        this.nazwaTechnologi = nazwaTechnologi;
    }

    public Kategoria getKategoria() {
        return kategoria;
    }

    public void setKategoria(Kategoria kategoria) {
        this.kategoria = kategoria;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tech tech = (Tech) o;
        return Objects.equals(nazwaTechnologi, tech.nazwaTechnologi) &&
                kategoria == tech.kategoria;
    }

    @Override
    public int hashCode() {

        return Objects.hash(nazwaTechnologi, kategoria);
    }
}
