package pl.codementors;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;


@Named
@RequestScoped
public class ZurawView {


    public static final String ZURAWIK = "\n" +
            "                               .....\n" +
            "                           .e$$$$$$$$$$$$$$e.\n" +
            "                          z$$ ^$$$$$$$$$$$$$$$$$.\n" +
            "                        .$$$* J$$$$$$$$$$$$$$$$$$$e\n" +
            "                       .$'  .$$$$$$$$$$$$$$$$$$$$$$*-\n" +
            "                      .$  $$$$$$$$$$$$$$$$***$$  .ee*\n" +
            "        z**$$        $$r ^**$$$$$$$$$** .e$$$$$$**\n" +
            "       * -ee$$      4$$$$.         .ze$$$$$**\n" +
            "       4 z$$$$$      $$$$$$$$$$$$$$$$$$$$$\n" +
            "       $$$$$$$$     .$$$$$$$$$$$**$$$$**\n" +
            "      z$$*    $$     $$$$P***     J$*$$c\n" +
            "      $$*      $$F   .$$$          $$ ^$$\n" +
            "      $$        *$$c.z$$$          $$   $$\n" +
            "      $P          $$$$$$$          4$F   4$\n" +
            "      dP            *$$$*           $$    '$r\n" +
            "      .$                            J$'     $'\n" +
            "       $                             $P     4$\n" +
            "       F                            $$      4$\n" +
            "                                    4$%      4$\n" +
            "                                    $$       4$\n" +
            "                                    d$'       $$\n" +
            "                                    $P        $$\n" +
            "                                    $$         $$\n" +
            "                                    4$%         $$\n" +
            "                                    $$          $$\n" +
            "                                    d$           $$\n" +
            "                                 sss$F         sss33\n";


    public String getZURAW() {
        return ZURAWIK;
    }
}
