package pl.codementors;

import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named
@ViewScoped
public class Edit implements Serializable {

    @Inject
    private CvDataStore store;

    private Cv cv;
    private List<SelectItem> technolo;
    
    public Cv getCv(){
        if (cv == null) {
            cv = store.getCV();
        }
        return cv;
    }

    public List<SelectItem> getTechnol() {
        if (technolo == null) {
            technolo = new ArrayList<SelectItem>();
            store.getTechno().forEach(techno -> {
                technolo.add(new SelectItem(techno, techno.getNazwaTechnologi() + " " + techno.getKategoria()));
            });
        }
        return technolo;
    }

    public void saveCv() {
        store.updateCv(cv);
    }
}
