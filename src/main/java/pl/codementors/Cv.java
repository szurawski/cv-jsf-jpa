package pl.codementors;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "cvjpa")
public class Cv implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

//    @Column
//    private String imie;
//    @Column
//    private String nazwisko;
//    @Column
//    private String tel;

    @Fetch(value = FetchMode.SUBSELECT)
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "doswiadczenie", referencedColumnName = "id")
    private List<Dosw> doswiadczenie;

    @Fetch(value = FetchMode.SUBSELECT)
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "edukacja", referencedColumnName = "id")
    private List<Edu> edukacja;

    @Fetch(FetchMode.SELECT)
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "umiejetnosci", referencedColumnName = "id")
    private List<Tech> umiejetnosci;

    @Fetch(value = FetchMode.SUBSELECT)
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "zainteresowania", joinColumns = @JoinColumn(name = "cv", referencedColumnName = "id"))
    private List<String> zainteresowania;


    public Cv() {
    }

    public Cv(String imie, String nazwisko, String tel, List<Dosw> doswiadczenie, List<Edu> edukacja, List<Tech> umiejetnosci, List<String> zainteresowania) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.tel = tel;
        this.doswiadczenie = doswiadczenie;
        this.edukacja = edukacja;
        this.umiejetnosci = umiejetnosci;
        this.zainteresowania = zainteresowania;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public List<Dosw> getDoswiadczenie() {
        return doswiadczenie;
    }

    public void setDoswiadczenie(List<Dosw> doswiadczenie) {
        this.doswiadczenie = doswiadczenie;
    }

    public List<Edu> getEdukacja() {
        return edukacja;
    }

    public void setEdukacja(List<Edu> edukacja) {
        this.edukacja = edukacja;
    }

    public List<Tech> getUmiejetnosci() {
        return umiejetnosci;
    }

    public void setUmiejetnosci(List<Tech> umiejetnosci) {
        this.umiejetnosci = umiejetnosci;
    }

    public List<String> getZainteresowania() {
        return zainteresowania;
    }

    public void setZainteresowania(List<String> zainteresowania) {
        this.zainteresowania = zainteresowania;
    }

    public void init() {
    }


}